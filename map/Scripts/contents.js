tileSize = 64;
maxZoom = 4;

var contents = 
[
	{
		id: "Map0",
		name: "Nibiru Reborn",
		players: Map0_playerData,
		beds: Map0_bedData,
		signs: Map0_signData,
		portals: Map0_portalData,
		views: Map0_viewData,
		blockStats: Map0_blockStats,
		worldStats: Map0_worldStats,
		worldVectors: Map0_worldVectors,
		layers:
		[
			{
				id: "LayerA",
				name: "Day",
				imageFormat: "png",
				isPng: "true"
			}
		]
	}
]
