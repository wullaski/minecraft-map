


var Map0_worldVectors =
{

	worldName: "woolbear",
	worldSizeInBytes: 0,
	numChunks: 82451,
	surfaceArea: "21.1",
	numPlayers: 4,
	spawnPosition: new WorldCoord(-100, 64, -64),
	origin: new google.maps.Point(2.0, 2.0),
	xAxis: new google.maps.Point(0.16666675, 0.0),
	yAxis: new google.maps.Point(0.0, 0.0),
	zAxis: new google.maps.Point(0.0, 0.16666651),
	mapXUnit: new google.maps.Point(6.0, 0.0),
	mapYUnit: new google.maps.Point(0.0, 6.0),
	mapMin: new google.maps.Point(-1152, -448),
	mapSize: new google.maps.Point(1664, 1664)
}