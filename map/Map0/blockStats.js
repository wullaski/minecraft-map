var Map0_blockStats=[
	{
		count: "26",
		name: "Acacia Door"
	},
	{
		count: "85",
		name: "Acacia Fence"
	},
	{
		count: "396,342",
		name: "Acacia Leaves"
	},
	{
		count: "11",
		name: "Acacia Sapling"
	},
	{
		count: "44,721",
		name: "Acacia Wood"
	},
	{
		count: "6",
		name: "Acacia Wood Slab"
	},
	{
		count: "43",
		name: "Acacia Wood Stairs"
	},
	{
		count: "264",
		name: "Acacia Wooden Plank"
	},
	{
		count: "4,013,194,719",
		name: "Air"
	},
	{
		count: "52,757,722",
		name: "Andesite"
	},
	{
		count: "3",
		name: "Anvil"
	},
	{
		count: "2,768",
		name: "Azure Bluet"
	},
	{
		count: "26",
		name: "Bed"
	},
	{
		count: "63,309,534",
		name: "Bedrock"
	},
	{
		count: "887,171",
		name: "Birch Leaves"
	},
	{
		count: "102,386",
		name: "Birch Wood"
	},
	{
		count: "28",
		name: "Black wool"
	},
	{
		count: "1,343",
		name: "Blue Orchid"
	},
	{
		count: "9",
		name: "Blue stained clay"
	},
	{
		count: "958",
		name: "Bookshelf"
	},
	{
		count: "2",
		name: "Brewing Stand"
	},
	{
		count: "8,778",
		name: "Brown Mushroom"
	},
	{
		count: "12,498",
		name: "Cactus"
	},
	{
		count: "655",
		name: "Carrots"
	},
	{
		count: "6",
		name: "Cauldron"
	},
	{
		count: "1,212",
		name: "Chest"
	},
	{
		count: "702",
		name: "Chiseled Sandstone"
	},
	{
		count: "145,366",
		name: "Clay"
	},
	{
		count: "14,034,878",
		name: "Coal Ore"
	},
	{
		count: "69,069",
		name: "Coarse Dirt"
	},
	{
		count: "94,289",
		name: "Cobblestone"
	},
	{
		count: "6",
		name: "Cobblestone Double Slab"
	},
	{
		count: "68",
		name: "Cobblestone Slab"
	},
	{
		count: "711",
		name: "Cobblestone Stairs"
	},
	{
		count: "134",
		name: "Cobblestone Wall"
	},
	{
		count: "2",
		name: "Dark Oak Door"
	},
	{
		count: "107",
		name: "Dark Oak Fence"
	},
	{
		count: "3",
		name: "Dark Oak Fence Gate"
	},
	{
		count: "433,582",
		name: "Dark Oak Leaves"
	},
	{
		count: "171,050",
		name: "Dark Oak Wood"
	},
	{
		count: "2,965",
		name: "Dark Prismarine"
	},
	{
		count: "9,395",
		name: "Dead Shrub"
	},
	{
		count: "291,055",
		name: "Diamond Ore"
	},
	{
		count: "48,501,447",
		name: "Diorite"
	},
	{
		count: "42,736,869",
		name: "Dirt"
	},
	{
		count: "2",
		name: "Dispenser"
	},
	{
		count: "45,110",
		name: "Double Tall Grass"
	},
	{
		count: "30,437",
		name: "Emerald Ore"
	},
	{
		count: "2",
		name: "Enchantment Table"
	},
	{
		count: "24",
		name: "End Portal Frame"
	},
	{
		count: "1",
		name: "Ender Chest"
	},
	{
		count: "250,784",
		name: "Fence"
	},
	{
		count: "1,227",
		name: "Fire"
	},
	{
		count: "9",
		name: "Flower Pot"
	},
	{
		count: "58",
		name: "Furnace"
	},
	{
		count: "13",
		name: "Gate"
	},
	{
		count: "909",
		name: "Glass Pane"
	},
	{
		count: "36",
		name: "Glowstone"
	},
	{
		count: "64",
		name: "Gold Block"
	},
	{
		count: "829,734",
		name: "Gold Ore"
	},
	{
		count: "50,651,252",
		name: "Granite"
	},
	{
		count: "7,002,039",
		name: "Grass"
	},
	{
		count: "20,281,015",
		name: "Gravel"
	},
	{
		count: "39",
		name: "Hay Block"
	},
	{
		count: "1",
		name: "Hay Block North/South"
	},
	{
		count: "244,073",
		name: "Hidden Silverfish"
	},
	{
		count: "4",
		name: "Hopper"
	},
	{
		count: "3,851",
		name: "Huge Brown Mushroom"
	},
	{
		count: "3,599",
		name: "Huge Red Mushroom"
	},
	{
		count: "435",
		name: "Ice"
	},
	{
		count: "887",
		name: "Iron Bar"
	},
	{
		count: "148",
		name: "Iron Door"
	},
	{
		count: "8,053,763",
		name: "Iron Ore"
	},
	{
		count: "1",
		name: "Iron Trapdoor"
	},
	{
		count: "35",
		name: "Jack-o-lantern"
	},
	{
		count: "229",
		name: "Ladder"
	},
	{
		count: "332,721",
		name: "Lapis Lazuli Ore"
	},
	{
		count: "5,096",
		name: "Large Fern"
	},
	{
		count: "7,634,799",
		name: "Lava"
	},
	{
		count: "3",
		name: "Lever"
	},
	{
		count: "579",
		name: "Light Gray Stained Glass"
	},
	{
		count: "75",
		name: "Light Gray Stained Glass Pane"
	},
	{
		count: "718",
		name: "Lilac"
	},
	{
		count: "37,595",
		name: "Lily Pad"
	},
	{
		count: "10",
		name: "Melon"
	},
	{
		count: "26",
		name: "Melon Stem"
	},
	{
		count: "1,365",
		name: "Mob Spawner"
	},
	{
		count: "32,332",
		name: "Mossy Cobblestone"
	},
	{
		count: "310",
		name: "Nether Brick"
	},
	{
		count: "101",
		name: "Nether Brick Fence"
	},
	{
		count: "38",
		name: "Nether Brick Stairs"
	},
	{
		count: "55",
		name: "Nether Wart"
	},
	{
		count: "42",
		name: "Netherrack"
	},
	{
		count: "1,383,119",
		name: "Oak Leaves"
	},
	{
		count: "4",
		name: "Oak Sapling"
	},
	{
		count: "105,824",
		name: "Oak Wood"
	},
	{
		count: "484",
		name: "Oak Wood East/West"
	},
	{
		count: "329",
		name: "Oak Wood North/South"
	},
	{
		count: "12",
		name: "Oak Wood Slab"
	},
	{
		count: "336,159",
		name: "Oak Wooden Plank"
	},
	{
		count: "100,709",
		name: "Obsidian"
	},
	{
		count: "195",
		name: "Orange Tulip"
	},
	{
		count: "683",
		name: "Orange stained clay"
	},
	{
		count: "2,796",
		name: "Oxeye Daisy"
	},
	{
		count: "818",
		name: "Peony"
	},
	{
		count: "151",
		name: "Pink Tulip"
	},
	{
		count: "6",
		name: "Piston Extension"
	},
	{
		count: "560",
		name: "Polished Diorite"
	},
	{
		count: "582",
		name: "Polished Granite"
	},
	{
		count: "12,205",
		name: "Poppy"
	},
	{
		count: "36",
		name: "Portal"
	},
	{
		count: "609",
		name: "Potatoes"
	},
	{
		count: "25",
		name: "Powered rails"
	},
	{
		count: "53,379",
		name: "Prismarine"
	},
	{
		count: "64,843",
		name: "Prismarine Bricks"
	},
	{
		count: "196",
		name: "Pumpkin"
	},
	{
		count: "24",
		name: "Pumpkin Stem"
	},
	{
		count: "70,726",
		name: "Rails"
	},
	{
		count: "3,755",
		name: "Red Mushroom"
	},
	{
		count: "228",
		name: "Red Tulip"
	},
	{
		count: "2,338,645",
		name: "Redstone Ore"
	},
	{
		count: "9",
		name: "Redstone Repeater"
	},
	{
		count: "23",
		name: "Redstone Torch"
	},
	{
		count: "128",
		name: "Redstone wire"
	},
	{
		count: "753",
		name: "Rose Bush"
	},
	{
		count: "20,579,298",
		name: "Sand"
	},
	{
		count: "19,462,013",
		name: "Sandstone"
	},
	{
		count: "170",
		name: "Sandstone Slab"
	},
	{
		count: "3,074",
		name: "Sandstone Stairs"
	},
	{
		count: "948",
		name: "Sea Lantern"
	},
	{
		count: "3",
		name: "Sign Post"
	},
	{
		count: "2",
		name: "Silver Carpet"
	},
	{
		count: "9,262",
		name: "Smooth Sandstone"
	},
	{
		count: "364,815",
		name: "Snow"
	},
	{
		count: "3,565",
		name: "Soil"
	},
	{
		count: "55",
		name: "Soul Sand"
	},
	{
		count: "44",
		name: "Spruce Door"
	},
	{
		count: "17",
		name: "Spruce Fence"
	},
	{
		count: "17",
		name: "Spruce Fence Gate"
	},
	{
		count: "424,856",
		name: "Spruce Leaves"
	},
	{
		count: "16",
		name: "Spruce Sapling"
	},
	{
		count: "56,329",
		name: "Spruce Wood"
	},
	{
		count: "38",
		name: "Spruce Wood North/South"
	},
	{
		count: "8",
		name: "Spruce Wood Slab"
	},
	{
		count: "322",
		name: "Spruce Wood Stairs"
	},
	{
		count: "766",
		name: "Spruce Wooden Plank"
	},
	{
		count: "6",
		name: "Sticky Piston"
	},
	{
		count: "904,611,708",
		name: "Stone"
	},
	{
		count: "41,448",
		name: "Stone Brick"
	},
	{
		count: "56",
		name: "Stone Brick Slab"
	},
	{
		count: "204",
		name: "Stone Brick Stairs"
	},
	{
		count: "100",
		name: "Stone Button"
	},
	{
		count: "269",
		name: "Stone Double Slab"
	},
	{
		count: "5",
		name: "Stone Pressure Plate"
	},
	{
		count: "771",
		name: "Stone Slab"
	},
	{
		count: "6,362",
		name: "Sugar Canes"
	},
	{
		count: "72",
		name: "TNT"
	},
	{
		count: "1,740,813",
		name: "Tall Grass"
	},
	{
		count: "5,215",
		name: "Torch"
	},
	{
		count: "2",
		name: "Trapdoor"
	},
	{
		count: "4",
		name: "Tripwire"
	},
	{
		count: "4",
		name: "Tripwire Hook"
	},
	{
		count: "269,940",
		name: "Vines"
	},
	{
		count: "7",
		name: "Wall Banner"
	},
	{
		count: "9",
		name: "Wall Sign"
	},
	{
		count: "118,695,950",
		name: "Water"
	},
	{
		count: "66,348",
		name: "Web"
	},
	{
		count: "383",
		name: "Wet Sponge"
	},
	{
		count: "2,021",
		name: "Wheat"
	},
	{
		count: "240",
		name: "White Tulip"
	},
	{
		count: "4",
		name: "White wool"
	},
	{
		count: "1",
		name: "Wooden Button"
	},
	{
		count: "370",
		name: "Wooden Door"
	},
	{
		count: "74",
		name: "Wooden Pressure Plate"
	},
	{
		count: "1,337",
		name: "Wooden Stairs"
	},
	{
		count: "60",
		name: "Workbench"
	},
	{
		count: "22,660",
		name: "Yellow Flower"
	}
];
